@sprint-2
Feature: As a user I want to be able to perform advanced company searches

Scenario: As a user I search for a company

	Given the user is logged in
	And performs an advanced business search for "AgileWare"
	Then the results shown on screen should be
		| AgileWare | agileware.org |
		| AgileWare | agileware.com |
		
@sprint-3
Scenario: As a user I search for a company

	Given the user is logged in
	And performs an advanced business search for "AgileWare"
	Then the results shown on screen should be
		| AgileWare | agileware.org |
		| AgileWare | agileware.com |
		
	