@sprint-1
Feature: As a user I want to be able to perform company searches

Scenario: As a user I search for AgileWare

	Given the user is logged in
	And performs a business search for "AgileWare"
	Then the results shown on screen should be
		| AgileWare | agileware.org |
		| AgileWare | agileware.com |
		
	
