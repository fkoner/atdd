package org.agileware.atdd;

import org.junit.runner.RunWith;

import cucumber.junit.Cucumber;

@RunWith(Cucumber.class)
@Cucumber.Options(features = "classpath:./company/advanced-search.feature", format = "html:target/cucumber")
public class JUnitWrapper {
}