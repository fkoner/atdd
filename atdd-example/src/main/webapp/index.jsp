<% for (int i = 0; i < 100; i++) { %>
<a id="<%=i%>" href="#" title="this is an example tooltip for element <%=i%>">element <%=i%></a><br />
<% } %>
<script>
	$('a[toolTip]').live('mouseOver', function() {
		$(this).tipTip({
			attribute : "toolTip"
		});
		$(this).trigger('mouseEnter');
	});
</script>