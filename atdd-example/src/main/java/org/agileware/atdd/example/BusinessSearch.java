package org.agileware.atdd.example;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/business")
public class BusinessSearch {

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public String searchForm() {
		return "search";
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public String signupForm(@RequestParam(value = "input", required = true) String term) {
		return "search-results";
	}
	
	public String test() {
		return "search-results";
	}
}
