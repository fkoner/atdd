Please note that the `verify` phase is included in the `install` phase so it's
safe to replace verify with install everytime it is used.

Unit test coverage reports can be found in target/site/coverage and integration
tests coverage in target/site/coverage-it.

--- Configuration

Cucumber feature files are located in src/test/features as specified in the 
cucumber.features.dir POM property.

Cucumber glue code is by default considered to be located in a package like 
${project.groupId}.${project.artifactId}.stepdefs as specified in the 
cucumber.glue.package POM property.

--- IDE Integration

If you want to use the JUnitWrapper within your IDE to run your automation tests 
it is important you specify the following two JVM arguments (consider the values
as examples):

-Dselenium.driver=org.openqa.selenium.ie.InternetExplorerDriver 
-Dwebapp.base.url=http://localhost:8080/atdd

--- Usage examples

> mvn clean verify
Builds the application running unit tests and calculating unit test coverage.

> mvn package cargo:run
Builds the application and starts it in the defined container. Maven will wait
for user interaction: useful for manual testing.

> mvn verify -P ci
Builds the application and runs the automation tests collecting coverage 
on unit and automation tests.

> mvn integration-test -P ci -Dfeature=some/thing.feature
Builds the application, starts the container, runs the scenarios in 
src/test/features/some/thing.feature, closes the container. Code coverage 
collected for unit and automation test.

> mvn verify -P ci -Dtags=@sprint-1
Builds the application, starts the container, runs all the automation tests 
tagged with "@sprint-1", closes the container. Code coverage collected for 
unit and automation test.

> mvn failsafe:integration-test -Dtags=~@sprint-1 -Dfeature=
Builds the application and runs all the automation tests not tagged with 
"@sprint-1" (the container is not started/closed). No coverage collected. 
To be used in conjuction with `mvn cargo:run`.

> mvn failsafe:integration-test  -Dfeature=some/thing.feature -Dtags=@sprint-1
Builds the application and runs all the scenarios in 
src/test/features/some/thing.feature tagged with "@sprint-1" (the container 
is not started/closed). No coverage collected. To use in conjuction with 
`mvn cargo:run`.

--- Profiles description

	ci
Used in continous integration environments, it automatically starts and stops 
the container, deploys the application and executes all the automation tests.